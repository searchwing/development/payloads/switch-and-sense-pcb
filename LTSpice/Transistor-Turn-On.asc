Version 4
SHEET 1 880 680
WIRE -32 -32 -208 -32
WIRE 192 -32 -32 -32
WIRE 192 0 192 -32
WIRE 192 96 192 80
WIRE -208 128 -208 -32
WIRE -32 176 -32 48
WIRE -32 176 -96 176
WIRE 144 176 -32 176
WIRE -32 224 -32 176
WIRE -208 256 -208 208
WIRE 192 256 192 192
WIRE -96 384 -96 176
WIRE -144 464 -208 464
FLAG -208 256 0
FLAG 192 256 0
FLAG -32 304 0
FLAG -208 544 0
FLAG -96 480 0
SYMBOL nmos 144 96 R0
SYMATTR InstName M1
SYMATTR Value BSC010N04LSI
SYMBOL voltage -208 112 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 18
SYMBOL res 176 -16 R0
SYMATTR InstName R1
SYMATTR Value 18
SYMBOL voltage -208 448 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value PULSE(0 10 2ms 0 0 10ms 0 1)
SYMBOL res -48 -48 R0
SYMATTR InstName R2
SYMATTR Value 110k
SYMBOL res -48 208 R0
SYMATTR InstName R3
SYMATTR Value 210k
SYMBOL nmos -144 384 R0
SYMATTR InstName M2
TEXT -240 280 Left 2 !.tran 10ms
