# switch-and-sense-pcb

The shortage/end of life of the Pixracer Autopilot triggered thoughts about the future setup of the electronics, especially the integration of the autopilot with the power module / power distribution. Different Autopilots have different features/requirements. This PCB is the first part of a modularized stackable electronics setup for the SearchWing drone.

More details are found in the [wiki](https://wiki.searchwing.org/en/home/Development/Electronics/switch-and-sense-pcb).

![2022-11-20_board-with-3dcomponents.png](pictures/2022-11-20_board-with-3dcomponents.png)

## Open with KiCAD

1. clone with ``git clone https://gitlab.com/searchwing/development/payloads/switch-and-sense-pcb.git``
2. update submodule with ``git submodule update --init``
3. open .kicad_pro file with kicad

